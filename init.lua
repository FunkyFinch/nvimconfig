-- nvim config
-- funkyfinch

-- Load Configs
require("config.options")
require("config.keymaps")
require("config.autocmds")


-- Bootstrap lazy.nvim
local lazypath = vim.fn.stdpath 'data' .. 'lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system {
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable", -- latest stable branch
		lazypath,
	}
end
vim.opt.rtp:prepend(lazypath)

--vim.cmd [[colorscheme unicorn]]

-- Load Plugins
require("lazy").setup("plugins")

