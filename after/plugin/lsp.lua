local lsp = require('lsp-zero')

lsp.preset({
   name = 'minimal',
   set_lsp_keymaps = true,
})

-- require('lspconfig').lua_ls.setup(lsp.nvim_lua_ls())

require('lspconfig').clangd.setup({})

lsp.setup()
