return {
   {
      "daschw/leaf.nvim",
      lazy = false,
      priority = 1000,
      config = function()
      require("leaf").setup({
		   underlineStyle = "undercurl",
		   transparent = true,
		   theme = "dark",
		   contrast = "low",
	      })

         -- vim.cmd([[colorscheme leaf]])
      end,
   },
}
