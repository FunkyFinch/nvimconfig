return {
   {
      dir="~/themes/unicorn",
      lazy = false,
      priority = 1000,
      config = function()
      vim.cmd([[colorscheme unicorn]])
      end,
   },
}
