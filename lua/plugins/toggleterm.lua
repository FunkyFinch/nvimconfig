return {
   {
      'akinsho/toggleterm.nvim',
      opts = {
         options = {
            size = 20,
            open_mapping = [[<c-\>]],
            hide_numbers = true,
            direction = 'horizontal',
         }
      }
   }
}
