return {
   {
      'akinsho/bufferline.nvim',
      --tag = 'v3.*.*',
      dependencies = {
         'nvim-tree/nvim-web-devicons',
      },
      config = function()
         require("bufferline").setup({
           options = {
              separator_style = "thin",
              offsets = {
                 {
                 filetype = "NvimTree",
                 text = "File Explorer",
                 highlight = "Directory",
                 text_align = "center"
                 }
              }
           }
         })
      end,
   }
}
