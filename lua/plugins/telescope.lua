return {
   {
      'nvim-telescope/telescope.nvim',
      tag = '0.1.1',
      dependencies = {
         'nvim-lua/plenary.nvim'
      },
      config = function()
         local builtin = require('telescope.builtin')
         vim.keymap.set('n', '<Leader>ff', builtin.find_files, {})
         vim.keymap.set('n', '<Leader>fg', builtin.git_files, {})
         vim.keymap.set('n', '<Leader>fs', function()
            builtin.grep_string({ search = vim.fn.input("Grep > ") })
         end)
         vim.keymap.set('n', '<Leader>ft', builtin.help_tags, {})
      end,
   }
}
