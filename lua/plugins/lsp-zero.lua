return {
   {
      "VonHeikemen/lsp-zero.nvim",
      branch = 'v1.x',
      dependencies = {
         --LSP Support
         {'neovim/nvim-lspconfig'},
         {'williamboman/mason.nvim', run = ":MasonUpdate"},
         {'williamboman/mason-lspconfig.nvim'},

         --Autocompletion
         {'hrsh7th/nvim-cmp'},
         {'hrsh7th/cmp-nvim-lsp'},
         {'hrsh7th/cmp-buffer'},           --Optional
         {'hrsh7th/cmp-path'},             --Optional
         {'saadparwaiz1/cmp_luasnip'},     --Optional
         {'hrsh7th/cmp-nvim-lua'},         --Optional

         --Snippets
         {'L3MON4D3/LuaSnip'},
         {'rafamadriz/friendly-snippets'},     --Optional
      },
   }
}
