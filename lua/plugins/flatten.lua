return {
   {
      "willothy/flatten.nvim",
      config = true,
      lazy = false,
      priority = 1001,

      opts = {
         callbacks = {
            pre_open = function()
               require("toggleterm").toggle(0)
            end,
            post_open = function(ibufnr, winnr, filetype)
               require("toggleterm").toggle(0)
               vim.api.nvim_set_current_win(winnr)
            end,
            block_end = function()
               require("toggleterm").toggle(0)
            end,
         },
         window = {
            open = "current",
         },
      },

      --config = function()
         --require("flatten").setup({
        --})
      --end,   

   }
}
