--Define Keymaps for nvim


vim.g.mapleader = " "

-- Write/Quit Commands
--vim.keymap.set("n", "<Leader>w", ":write<CR>", { noremap = true, silent = true })
--vim.keymap.set("n", "<Leader>q", ":quit<CR>", { noremap = true, silent = true })
--vim.keymap.set("n", "<Leader>wq", ":wq<CR>", { noremap = true, silent = true })
--vim.keymap.set("n", "<Leader><Leader>wq", ":wq!<CR>", { noremap = true, silent = true })

-- Set <Leader> + t to ToggleTerm with default settings
vim.keymap.set("n", "<Leader>t", ":ToggleTerm<CR>", { noremap = true, silent = true })
vim.keymap.set("t", "<Leader><Leader>t", [[<C-\><C-n>]], { noremap = true, silent = true })



vim.keymap.set("n", "<Leader>d", ":NvimTreeToggle<CR>", { noremap = true, silent = true })
